FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY hello.txt /var/www/hello.txt

RUN rm -rf /etc/nginx/conf.d/*

COPY conf.d/ /etc/nginx/conf.d/

WORKDIR /var/www/
EXPOSE 80

