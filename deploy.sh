#!/usr/bin/env bash

function docker_hub_authentication {
    docker login -u ${DOCKER_HUB_ID} -p ${DOCKER_HUB_PASSWORD}
}

# Build docker image based and push to AWS ECR
function build_and_push_docker_image {
    docker_hub_authentication

    docker build -t ${DOCKER_HUB_REGISTRY_URL}:latest .
    docker push ${DOCKER_HUB_REGISTRY_URL}:latest
}
