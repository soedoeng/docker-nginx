#!/bin/bash
git pull origin master
docker pull jsimanjuntak/bibit:latest

docker rm bibit  -f || true
echo -e '\e[1m\e[34m\nRestarting service..\e[0m\n'
#stop the application
docker run -d --rm --name bibit -p 80:80 jsimanjuntak/bibit:latest
#make sure close port 8080

